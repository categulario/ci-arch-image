FROM archlinux:latest

RUN pacman -Syu --needed --noconfirm git openssh binutils

RUN useradd -d /home/builder -m builder

USER builder

CMD ["/usr/bin/bash"]
